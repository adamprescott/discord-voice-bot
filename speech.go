package main

import (
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
)

type SpeechGateway interface {
	init()
	SendVoiceData(sender *SpeechSender)
}

type SpeechSender struct {
	SSRC      uint32
	PCM       []int16
	Timestamp uint32
	DGV       *discordgo.VoiceConnection
}

type Processor struct {
	Gateway *SpeechGateway
}

func (p *Processor) init() {
	gateway, err := NewSpeechGateway(AppConfig.Service)
	if err != nil {
		panic(err)
	}
	p.Gateway = &gateway
}

func (p *Processor) ProcessSender(sender <-chan *SpeechSender) {
	for {
		toSend, ok := <-sender
		if !ok {
			fmt.Println("Sender Channel Closed")
			return
		}

		gateway := *p.Gateway
		go gateway.SendVoiceData(toSend)
	}
}

func NewSpeechGateway(t string) (SpeechGateway, error) {
	switch t {
	case "google":
		gw := new(GoogleGateway)
		gw.init()
		return gw, nil
	case "echo":
		gw := new(EchoGateway)
		gw.init()
		return gw, nil
	default:
		return nil, errors.New(fmt.Sprintf(`Gateway "%s" unknown`, t))
	}
}
