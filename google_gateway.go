package main

import (
	"cloud.google.com/go/speech/apiv1"
	"context"
	"fmt"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
	"log"
	"encoding/binary"
	"bytes"
	"io"
	"cloud.google.com/go/bigtable/bttest"
)

type GoogleGateway struct {
	SRC    *speechpb.StreamingRecognitionConfig
	Stream *speechpb.Speech_StreamingRecognizeClient
}

func (g *GoogleGateway) init() {
	ctx := context.Background()

	client, err := speech.NewClient(ctx)
	if err != nil {
		fmt.Println(err)
	}

	stream, err := client.StreamingRecognize(ctx)
	if err != nil {
		fmt.Println(err)
	}

	// Send initial config frame
	if err := stream.Send(&speechpb.StreamingRecognizeRequest{
		StreamingRequest: &speechpb.StreamingRecognizeRequest_StreamingConfig{
			StreamingConfig: &speechpb.StreamingRecognitionConfig{
				Config: &speechpb.RecognitionConfig{
					Encoding: speechpb.RecognitionConfig_LINEAR16,
					SampleRateHertz: 48000,
					LanguageCode: "en-US",
				},
			},
		},
	}); err != nil {
		log.Fatal(err)
	}

	g.Stream = &stream
}

func (g *GoogleGateway) SendVoiceData(sender *SpeechSender) {

	stream := *g.Stream

	go func() {
		buf := new(bytes.Buffer)
		err := binary.Write(buf, binary.LittleEndian, sender.PCM)
		if err != nil {
			log.Fatal(err)
		}

		// Todo Figure this damn thing out

		for {

			request := &speechpb.StreamingRecognizeRequest{
				StreamingRequest: &speechpb.StreamingRecognizeRequest_AudioContent{
					AudioContent: ,
				},
			}
			if err := stream.Send(request); err != nil {
				log.Printf("Could not send audio: %v", err)
			}
		}
	}()

	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("Cannot stream results: %v", err)
		}
		if err := resp.Error; err != nil {
			// Workaround while the API doesn't give a more informative error.
			if err.Code == 3 || err.Code == 11 {
				log.Print("WARNING: Speech recognition request exceeded limit of 60 seconds.")
			}
			log.Fatalf("Could not recognize: %v", err)
		}
		for _, result := range resp.Results {
			fmt.Printf("Result: %+v\n", result)
		}
	}
}
