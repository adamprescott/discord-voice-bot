package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"os"
	"os/signal"
	"syscall"
)

var (
	AppConfig      *Config
	DiscordSession *discordgo.Session
	UserToSSRC     map[int]string
)

func init() {
	AppConfig = GetConfig()
	if discord, err := discordgo.New("Bot " + AppConfig.Discord.Token); err != nil {
		fmt.Println(err)
		panic("Discord Error")
	} else {
		DiscordSession = discord
	}

}

func main() {
	if err := DiscordSession.Open(); err != nil {
		fmt.Println(err)
		panic("Discord Error")
	}

	dgv, err := DiscordSession.ChannelVoiceJoin(
		AppConfig.Discord.GuildId,
		AppConfig.Discord.VoiceChannel,
		true,
		false)
	if err != nil {
		fmt.Println(err)
		panic("Discord Error")
	}

	dgv.AddHandler(func(vc *discordgo.VoiceConnection, vs *discordgo.VoiceSpeakingUpdate) {
		//UserToSSRC[vs.SSRC] = vs.UserID
		if vs.Speaking {
			//fmt.Println(fmt.Sprintf("User %s is speaking with SSRC %d", vs.UserID, vs.SSRC))
		} else {
			//fmt.Println(fmt.Sprintf("User %s has stopped speaking", vs.UserID))
		}
	})

	// Cleanly close down the Discord session.
	defer func() {
		sc := make(chan os.Signal, 1)
		signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
		<-sc
		dgv.Close()
		DiscordSession.Close()
	}()

	process(dgv)
}

func process(v *discordgo.VoiceConnection) {
	recv := make(chan *discordgo.Packet, 2)
	go ReceivePCM(v, recv)

	Processor := new(Processor)
	Processor.init()
	send := make(chan *SpeechSender, 2)
	go Processor.ProcessSender(send)

	for {
		p, ok := <-recv
		if !ok {
			fmt.Println("Not OK")
			return
		}

		newSender := new(SpeechSender)
		newSender.SSRC = p.SSRC
		newSender.Timestamp = p.Timestamp
		newSender.PCM = p.PCM
		newSender.DGV = v

		send <- newSender
	}
}
