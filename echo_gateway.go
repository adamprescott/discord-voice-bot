package main

import (
	"fmt"
	"layeh.com/gopus"
)

type EchoGateway struct {
	encoder *gopus.Encoder
}

func (e *EchoGateway) init() {
	opusEncoder, err := gopus.NewEncoder(frameRate, channels, gopus.Audio)
	if err != nil {
		OnError("NewEncoder Error", err)
		return
	}
	e.encoder = opusEncoder
}

func (e *EchoGateway) SendVoiceData(s *SpeechSender) {
	opus, err := e.encoder.Encode(s.PCM, frameSize, maxBytes)
	if err != nil {
		OnError("Encoding Error", err)
		return
	}
	fmt.Println("Echoing Voice Data")
	s.DGV.OpusSend <- opus
}
