package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
)

type Config struct {
	Service     string `toml:"speech_service"`
	Discord     DiscordConfig
	GoogleCloud GoogleConfig `toml:"google_cloud"`
}

type DiscordConfig struct {
	AuthType     string `toml:"auth_type"`
	Username     string
	Password     string
	Token        string
	GuildId      string `toml:"guild_id"`
	VoiceChannel string `toml:"voice_channel"`
}

type GoogleConfig struct {
	Token  string
	Secret string
}

func GetConfig() *Config {
	var config Config
	fmt.Println("Parsing Config")
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		fmt.Println(err)
		return nil
	}

	fmt.Println("Config Parsed", config)
	return &config
}
